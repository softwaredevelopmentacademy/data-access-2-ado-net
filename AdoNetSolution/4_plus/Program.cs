﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_plus
{
    class Program
    {
        const string connectionString = "jakis tam connection string";

        static void Main(string[] args)
        {
            Console.WriteLine("start");
            FirstStep();
            SecStep();

            var kitt = new Car();
            kitt.Color = "black";

            var viper = new Car("red");

            var cars = new List<Car>();
            cars.Add(kitt);
            cars.Add(viper);

            foreach (var car in cars)
            {
                car.ShowColor();
                //Console.WriteLine($"IsEnginStart: {car.IsEnginStart}");
                car.StartEngin();
                Console.WriteLine($"IsEnginStart: {car.IsEnginStart}");
            }

            Console.WriteLine("stop ostatni");
            cars.Last().StopEngin();

            foreach (var car in cars)
            {
                car.ShowColor();
                Console.WriteLine($"IsEnginStart: {car.IsEnginStart}");
            }

            Console.WriteLine("koniec");
            Console.ReadKey();
        }

        private static void SecStep()
        {
            Console.WriteLine("Second Step");
            Console.WriteLine(connectionString);
        }

        private static void FirstStep()
        {
            Console.WriteLine("First Step");
        }
    }
}
