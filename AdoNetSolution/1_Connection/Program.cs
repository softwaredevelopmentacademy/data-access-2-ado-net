﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace _1_Connection
{
    class Program
    {
        static void Main(string[] args)
        {
            DbConnection();

            //Insert();
            Select();
            //Update();
            //Delete();
            //ExecuteScalar();
            
            // prevent closing the console
            Console.ReadKey();
        }

        private static void DbConnection()
        {
            var connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";

            //var sql = "SELECT name FROM sysobjects WHERE xtype='U' order by 1";
            var sql = "select 1;";

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            try
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($"{reader[0]}");
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            /*
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        //Console.WriteLine($"{reader[0]}");
                        Console.WriteLine($"{reader[0]}");
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            */
        }

        private static void Insert()
        {
            
            var connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";
/*
            var sql = @"insert into [dbo].[Employees] (FirstName, LastName, Title, TitleOfCourtesy, BirthDate, HireDate, [Address], City, PostalCode, Country) 
                        values('Luke', 'Skywalker', 'jedi', 'Mr.', '1951-09-25', SYSDATETIME(), 'Tatooine 13', 'Tatooine', '11222', 'USA')";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            */
            
            var sql = @"insert into [dbo].[Employees] (LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, [Address], City, PostalCode, Country) 
                        values(@lastName, @firstName, @title, @titleOfCourtesy, @birthDate, @hireDate, @address, @city, @postalCode, @country)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    var sqlLastNameParam = new SqlParameter("@lastName", DbType.String);
                    sqlLastNameParam.Value = "Solo";
                    command.Parameters.Add(sqlLastNameParam);

                    command.Parameters.Add(new SqlParameter("@firstName", DbType.String) { Value = "Han" });
                    command.Parameters.Add(new SqlParameter("@title", DbType.String) { Value = "smuggler" });
                    command.Parameters.Add(new SqlParameter("@titleOfCourtesy", DbType.String) { Value = "Mr." });
                    command.Parameters.Add(new SqlParameter("@birthDate", DbType.DateTime) { Value = new DateTime(1942, 07, 13) });
                    command.Parameters.Add(new SqlParameter("@hireDate", DbType.DateTime) { Value = DateTime.Now });
                    command.Parameters.Add(new SqlParameter("@address", DbType.String) { Value = "millennium falcon" });
                    command.Parameters.Add(new SqlParameter("@city", DbType.String) { Value = "millennium" });
                    command.Parameters.Add(new SqlParameter("@postalCode", DbType.String) { Value = "33377" });
                    command.Parameters.Add(new SqlParameter("@country", DbType.String) { Value = "USA" });

                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            

            /*
            //SCOPE_IDENTITY
            var sql = @"insert into [dbo].[Employees] (LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, [Address], City, PostalCode, Country) 
                       values(@lastName, @firstName, @title, @titleOfCourtesy, @birthDate, @hireDate, @address, @city, @postalCode, @country); select scope_identity();";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    var sqlLastNameParam = new SqlParameter("@lastName", DbType.String);
                    sqlLastNameParam.Value = "Solo";
                    command.Parameters.Add(sqlLastNameParam);

                    command.Parameters.Add(new SqlParameter("@firstName", DbType.String) { Value = "Han" });
                    command.Parameters.Add(new SqlParameter("@title", DbType.String) { Value = "smuggler" });
                    command.Parameters.Add(new SqlParameter("@titleOfCourtesy", DbType.String) { Value = "Mr." });
                    command.Parameters.Add(new SqlParameter("@birthDate", DbType.DateTime) { Value = new DateTime(1942, 07, 13) });
                    command.Parameters.Add(new SqlParameter("@hireDate", DbType.DateTime) { Value = DateTime.Now });
                    command.Parameters.Add(new SqlParameter("@address", DbType.String) { Value = "millennium falcon" });
                    command.Parameters.Add(new SqlParameter("@city", DbType.String) { Value = "millennium" });
                    command.Parameters.Add(new SqlParameter("@postalCode", DbType.String) { Value = "33377" });
                    command.Parameters.Add(new SqlParameter("@country", DbType.String) { Value = "USA" });

                    connection.Open();
                    var newId = command.ExecuteScalar();

                    Console.WriteLine($"new id: {newId}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            */
        }

        private static void Select()
        {
            var connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";

            var sql = "select * from Employees;";

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            try
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($@"
                        Id: {reader[0]} 
                        Last name: {reader[1]} 
                        First name: {reader[2]} 
                        Title: {reader[3]} 
                        Birth Date: { ((DateTime)reader[6]).ToLongDateString() }");
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void Update()
        {
            var connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";

            var sql = @"update [dbo].[Employees] set FirstName = @firstName where LastName = @lastName";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    command.Parameters.Add(new SqlParameter("@firstName", DbType.String) { Value = "Leia" });
                    command.Parameters.Add(new SqlParameter("@lastName", DbType.String) { Value = "Skywalker" });

                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void Delete()
        {
            var connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";

            var sql = @"delete [dbo].[Employees] where FirstName = @firstName and LastName = @lastName";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    command.Parameters.Add(new SqlParameter("@firstName", DbType.String) { Value = "Leia" });
                    command.Parameters.Add(new SqlParameter("@lastName", DbType.String) { Value = "Skywalker" });

                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void ExecuteScalar()
        {
            var connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";

            var sql = @"select count(1) from [dbo].[Employees]";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    connection.Open();
                    var count = command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
