﻿using System;
using System.Data.SqlClient;
using Dapper;
using System.Collections;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;

namespace _3_Dapper
{
    class Program
    {
        const string connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";

        static void Main(string[] args)
        {
            //GetData();

            Console.Write("Id:");
            var id = Console.ReadLine();

            Console.Write("Last Name:");
            var lastName = Console.ReadLine();
            //GetByLastName(lastName);

            Update(int.Parse(id), lastName);

            Console.ReadKey();
        }

        private static void GetByLastName(string lastName)
        {
            var employe = new Employe();
            using (var connection = new SqlConnection(connectionString))
            {
                employe = connection.Query<Employe>(
                    "Select * From Employees WHERE LastName = @lastName",
                    new { lastName })
                    .FirstOrDefault();
            }

            if (employe == null)
            {
                Console.WriteLine($"no data");
                return;
            }

            Console.WriteLine($"First name: {employe.FirstName} Last name: {employe.LastName} Birthday: {employe.BirthDate.ToShortDateString()}");
        }

        private static void GetData()
        {
            var employees = new List<Employe>();
            using (var connection = new SqlConnection(connectionString))
            {
                employees = connection.Query<Employe>("Select * From Employees")
                    .ToList();
            }
            var limitDate = DateTime.Now.AddDays(-20);
            employees = employees.Where(e => e.BirthDate >= limitDate).ToList();

            employees.ForEach(emp =>
            {
                Console.WriteLine($"First name: {emp.FirstName} Last name: {emp.LastName} Birthday: {emp.BirthDate.ToLongDateString()}");
            });
        }

        //static void Main(string[] args)
        //{
        //    GetByDapper();
        //    /*FindByDapper("Skywalker");
        //    Update();*/
        //    Console.ReadKey();
        //}

        private static void GetByDapper()
        {
            var employees = new List<Employe>();
            using (var connection = new SqlConnection(connectionString))
            {
                employees = connection.Query<Employe>("Select * From Employees")
                    .ToList();
            }

            employees.ForEach(emp =>
            {
                Console.WriteLine($"First name: {emp.FirstName} Last name: {emp.LastName} Birthday: {emp.BirthDate.ToShortDateString()}");
            });
        }

        private static void Update(int id, string lastName)
        {
            string sqlQuery = @"UPDATE Employees 
                        SET FirstName = @FirstName, 
                            LastName = @LastName, 
                            Title = @Title, 
                            HireDate = @HireDate,           
                            BirthDate = @BirthDate 
                      WHERE EmployeeID = @EmployeeID";

            var employe = new Employe()
            {
                EmployeeID = id,
                FirstName = "Lord",
                LastName = lastName,
                Title = "sith lord",
                HireDate = DateTime.Now,
                BirthDate = DateTime.Now
            };

            using (var connection = new SqlConnection(connectionString))
            {
                int rowsAffected = connection.Execute(sqlQuery, employe);
            }
        }














        private static void FindByDapper(string lastName)
        {
            var employe = new Employe();
            using (var connection = new SqlConnection(connectionString))
            {
                //employees = connection.Query<Employe>("Select * From Employees").ToList();
                employe = connection.Query<Employe>("Select * From Employees WHERE LastName = @lastName", new { lastName })
                    .FirstOrDefault();
            }

            if (employe == null)
            {
                Console.WriteLine($"no date");
                return;
            }

            Console.WriteLine($"First name: {employe.FirstName} Last name: {employe.LastName} Birthday: {employe.BirthDate.ToShortDateString()}");
        }

        private static void Test()
        {
            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            //watch.Stop();
            //watch.ElapsedMilliseconds;
        }
    }
}
